﻿using System;

namespace FourthTask
{
    public class Product
    {
        public string productName;
        public int productPrice;
        public int discount;
        public double gst;
        public int quantity;
        public double totalbill;

        public void GetTotalPrice()
        {
            productName = "Laptop";
            productPrice = 80000;
            gst = 0.18 * productPrice;
            Console.WriteLine("Enter The Quantity: ");
            quantity = int.Parse(Console.ReadLine());
            discount = productPrice >= 80000 ? 100 : 0;
            totalbill = (quantity * productPrice) - discount + gst;
            Console.WriteLine($"Your Total Bill = {totalbill}");
        }
    }
}
